import * as firebase from 'firebase';
import { firebaseConfig } from './config.js';

const firebaseApp = firebase.initializeApp(firebaseConfig);
const rootRef = firebaseApp.database().ref();

export function getData(child) {			//child: 位置
	return rootRef.child(child).once('value').then((json) => {
		return json;
	});
}

export function pushData(child, data) {	//child: 位置，data: JSON資料
	return rootRef.child(child).update(data);
}

//import randomatic from 'randomatic';
//console.log(randomatic('aA0', 20));
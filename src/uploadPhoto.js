import * as ImagePicker from 'react-native-image-picker';
import { FileUpload } from 'NativeModules';
import { serverConfig } from './config.js';
import { pushData, getData } from './firebase.js';

let options = {
  title: '請選擇圖片',
  cancelButtonTitle: '取消',
  takePhotoButtonTitle: '拍照',
  chooseFromLibraryButtonTitle: '相簿'
};

export function uploadPhoto(server, serverPath, dataPath, dataJson, callback) {
	ImagePicker.showImagePicker(options, (response) => {
	  if (response.didCancel) {
	    console.log('取消選擇！');
	  }
	  else if (response.error) {
	    alert("ImagePicker發生錯誤：" + response.error);
	  }
	  else {
	  	console.log(response);
	  	uploadToServer(response.fileName, response.path, response.type, server, serverPath, dataPath, dataJson, callback);
	  }
	});
}

function uploadToServer(fileName, filePath, fileType, server, serverPath, dataPath, dataJson, callback) {
	let photoUriPath = serverPath + '/' + fileName;
	let obj = {
		uploadUrl: server + serverPath,
		method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Authorization': serverConfig
    },
    fields: {
        'hello': 'world',
    },
    files: [
      {
        filename: fileName,
        filepath: filePath,
        filetype: fileType,
      }
    ]
	}
	let Json = {};
	Json[dataJson] = photoUriPath;

  FileUpload.upload(obj, (err, result) => {
    console.log('upload:', err, result);
    if(result != null){
    	if(result.status == 200){
				pushData(dataPath, Json);
				callback();
    	}
    }
  });
}

export function uploadPhotoList(server, serverPath, dataPath, index, callback) {
  ImagePicker.showImagePicker(options, (response) => {
    if (response.didCancel) {
      console.log('取消選擇！');
    }
    else if (response.error) {
      alert("ImagePicker發生錯誤：" + response.error);
    }
    else {
      console.log(response);
      uploadListToServer(response.fileName, response.path, response.type, server, serverPath, dataPath, index, callback);
    }
  });
}

function uploadListToServer(fileName, filePath, fileType, server, serverPath, dataPath, index, callback) {
  let photoUriPath = serverPath + '/' + fileName;
  let obj = {
    uploadUrl: server + serverPath,
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Authorization': serverConfig
    },
    fields: {
        'hello': 'world',
    },
    files: [
      {
        filename: fileName,
        filepath: filePath,
        filetype: fileType,
      }
    ]
  }
  getData(dataPath).then(data => {
    let Json = data.val();
    Json[index] = photoUriPath;

    FileUpload.upload(obj, (err, result) => {
      console.log('upload:', err, result);
      if(result != null){
        if(result.status == 200){
          pushData(dataPath, Json);
          callback();
        }
      }
    });
  });
}

export function uploadFacePhotoList(server, serverPath, dataPath, index, callback) {
  ImagePicker.showImagePicker(options, (response) => {
    if (response.didCancel) {
      console.log('取消選擇！');
    }
    else if (response.error) {
      alert("ImagePicker發生錯誤：" + response.error);
    }
    else {
      console.log(response);
      uploadFaceListToServer(response.fileName, response.path, response.type, server, serverPath, dataPath, index, callback);
    }
  });
}

function uploadFaceListToServer(fileName, filePath, fileType, server, serverPath, dataPath, index, callback) {
  let photoUriPath = serverPath + '/' + fileName;
  let obj = {
    uploadUrl: server + serverPath,
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Authorization': serverConfig
    },
    fields: {
        'hello': 'world',
    },
    files: [
      {
        filename: fileName,
        filepath: filePath,
        filetype: fileType,
      }
    ]
  }
  getData(dataPath).then(data => {
    let Json = data.val();
    Json[index] = photoUriPath;

    FileUpload.upload(obj, (err, result) => {
      console.log('upload:', err, result);
      if(result != null){
        if(result.status == 200){
          pushData(dataPath, Json);
          callback(photoUriPath);
        }
      }
    });
  });
}
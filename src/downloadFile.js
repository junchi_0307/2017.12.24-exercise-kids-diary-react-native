import RNFS from 'react-native-fs';

export function downloadFile(fromuri, navigator) {
	let fileName = fromuri.substring(fromuri.lastIndexOf('/')+1);
	const downloadDest = `${RNFS.ExternalStorageDirectoryPath}/Pictures/${fileName}`;

	const options = {
      fromUrl: fromuri,
      toFile: downloadDest,
      background: true,
      begin: (res) => {
          console.log('begin', res);
          console.log('contentLength:', res.contentLength / 1024 / 1024, 'M');
      },
      progress: (res) => {

          let pro = res.bytesWritten / res.contentLength;

          /*this.setState({
              progressNum: pro,
          });*/
      }
  };
  try {
      const ret = RNFS.downloadFile(options);
      ret.promise.then(res => {
          console.log('success', res);

          console.log('file://' + downloadDest);

          navigator.showSnackbar({
            text: 'Download success',
            textColor: '#AAA'
          });

      }).catch(err => {
          console.log('err', err);
      });
  }
  catch (e) {
      console.log(error);
  }
}
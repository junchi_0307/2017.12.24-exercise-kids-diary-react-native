//import { menu, home, back } from '../complex.js';
//menu(this.props.navigator, this.props.user, this.props.permissions)
export function menu(navigator, user, permissions) {
  return navigator.resetTo({
    screen: 'Menu',
    passProps: {
      user: user,
      permissions: permissions
    }
  });
}

//back(this.props.navigator)
export function back(navigator) {
  return navigator.pop();
}

//home(this.props.navigator, this.props.user, this.props.permissions)
export function home(navigator, user, permissions) {
  if(permissions == 1) {
    return navigator.resetTo({
      screen: 'Home',
      passProps: {
        user: user,
        permissions: permissions
      }
    });
  }else{
    return navigator.resetTo({
      screen: 'Students',
      passProps: {
        user: user,
        permissions: permissions
      }
    });
  }
}
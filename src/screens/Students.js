import React, { Component } from 'react';
import {
	StyleSheet,
	View,
	TouchableOpacity,
	Image,
	Text,
	ScrollView,
} from 'react-native';
import GridList from 'react-native-grid-list';
import Ionicons from 'react-native-vector-icons/Ionicons';

import { menu, home, back } from '../complex.js';
import { getData } from '../firebase.js';

export default class example extends Component {
	constructor(props) {
		super(props);
		this.props.navigator.toggleNavBar({
			to: 'hidden',
			animated: false,
		});
    this.student.bind(this);
		this.logout.bind(this);
		this.state = {
      permissions: -1
		};
	}

  logout(){
    this.props.navigator.resetTo({
      screen: 'Mean'
    });
  }

	componentDidMount() {
    getData('ip').then((json) => {
      this.setState({server: json.val()});
    });
    getData('users/' + this.props.user + '/class').then(json => {
      let classroom = json.val();
      this.setState({class: classroom});
    }).then(() => {
      getData('class/' + this.state.class + '/students').then((json) => {
        let studentsJson = json.val();
        let students = [];

        for(let i=0 ; i<studentsJson.length ; i++){
          students.push({user: studentsJson[i]});
        }
        students.push({user: 'add'});

        for(let i=0 ; i<studentsJson.length ; i++){
          getData('users/' + studentsJson[i]).then((json) => {
            students[i]['photo'] = this.state.server + json.val()['personalPhoto'][0];
            students[i]['name'] = json.val()['name'];
          });
        }

        setTimeout(() => {
          this.setState({
            students: students,
            permissions: this.props.permissions
          });
          console.log(this.state);
        }, 500);
      });
    });
  }

  student(item) {
    if(item.user == 'add'){
      this.props.navigator.push({
        screen: 'Sign',
        passProps: {
          user: this.props.user,
          permissions: this.props.permissions,
          class: this.state.class
        }
      });
    }else{
      this.props.navigator.push({
        screen: 'Home',
        passProps: {
          user: this.props.user,
          permissions: this.props.permissions,
          student: item.user
        }
      });
    }
  }

	render() {
		return (
			<View>
        <ScrollView>
        <Image style={{width:360, height: 1066}} source={require('../../img/back4.png')} />
        <TouchableOpacity onPress={() => home(this.props.navigator, this.props.user, this.props.permissions)} style={{position: 'absolute',left:10,top:0}}>
          <Image style={{width:92*0.7, height: 69*0.7}} source={require('../../img/house.png')} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => menu(this.props.navigator, this.props.user, this.props.permissions)} style={{position: 'absolute',right:3,top:7}}>
          <Image style={{width:138*0.7, height: 56*0.7}} source={require('../../img/menu3.png')} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.logout()} style={{position: 'absolute',left:75,top:15}}>
          <Image style={{width:45, height:35}} source={require('../../img/gobback.png')} />
        </TouchableOpacity>
        {this.props.permissions == 0?
          <View style={{width: 360, top: 50, height: 1016, position: 'absolute'}}>
            <GridList
              renderItem={({item, index}) => 
                <TouchableOpacity
                  style={{width: 180, height: 180}}
                  onPress={() => this.student(item)}
                  key={index}
                >
                    {item.user == 'add'?
                      <View style={{width: 180, height: 180, alignItems: 'center', justifyContent: 'center'}}>
                        <View style={{width: 140, height: 140, alignItems: 'center', justifyContent: 'center', borderRadius: 75, borderStyle: 'dashed', borderWidth: 1}}>
                          <Ionicons name='ios-add' size={80} color='#999' />
                        </View>
                        <View style={{width: 140, height: 40, alignItems: 'center', justifyContent: 'center', borderRadius: 20, borderStyle: 'dashed', borderWidth: 1}}>
                        </View>
                      </View>
                      :
                      <View style={{width: 180, height: 180, alignItems: 'center', justifyContent: 'center'}}>
                        <View style={{width: 140, height: 140, alignItems: 'center', justifyContent: 'center', borderRadius: 75, backgroundColor: 'white'}}>
                          <Image source={{uri: item.photo}} style={{borderRadius: 75, width: 140, height: 140}} />
                        </View>
                        <View style={{width: 140, height: 40, alignItems: 'center', justifyContent: 'center', borderRadius: 20, backgroundColor: 'white'}}>
                          <Text>{item.name}</Text>
                        </View>
                      </View>
                    }
                </TouchableOpacity>
              }
              data={this.state.students}
              numColumns={2}
            />
          </View>
        : null
        }
        </ScrollView>
      </View>
		);
	}
}

const styles = StyleSheet.create({

});
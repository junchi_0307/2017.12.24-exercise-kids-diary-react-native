import React, { Component } from 'react';
import { Hoshi } from 'react-native-textinput-effects';
import {
	StyleSheet,
	View,
	Image,
	Text,
	TouchableOpacity,
	TextInput,
	ScrollView
} from 'react-native';
import { getData, pushData } from '../firebase.js';
import { menu, back, home } from '../complex.js';

export default class example extends Component {
	constructor(props) {
		super(props);
		this.props.navigator.toggleNavBar({
			to: 'hidden',
			animated: false,
		});
    this.student.bind(this);
    this.teacher.bind(this);
		this.state = {
			title: '',
			date: '',
			article:''
		};
	}

	componentDidMount() {
    getData('ip').then((json) => {
      this.setState({server: json.val()});
    });
    getData('class/' + this.props.class + '/news/' + this.props.index).then((json) => {
	    let data = json.val();

      this.setState({
        permissions: this.props.permissions,
        title: data.title,
        date: data.date,
        article: data.article
      });
    });
	}

	teacher() {
		return(
			<View style={{position: 'absolute', width: 285, height: 400, top: 80}}>
				<View style={{position: 'absolute',backgroundColor: 'white', borderRadius: 10, width: 285, height:50, top: 0, borderColor: '#AAA', borderWidth:1}}>
					<Hoshi
						label='標題:'
						borderColor='#AAA' 
						onChangeText={(temp) => {
							this.setState({title: temp});
							pushData('class/' + this.props.class + '/news/' + this.props.index, {
								title: temp,
								date: this.state.date,
								article: this.state.article
							});
						}}
	   				value={this.state.title}
	   			/>
	   		</View>
	   		<View style={{position: 'absolute',backgroundColor: 'white', borderRadius: 10, width: 285, height:50, top: 50, borderColor: '#AAA', borderWidth:1}}>
					<Hoshi
						label='日期:'
						borderColor='#AAA'
						onChangeText={(temp) => {
							this.setState({date: temp});
							pushData('class/' + this.props.class + '/news/' + this.props.index, {
								title: this.state.title,
								date: temp,
								article: this.state.article
							});
						}}
	   				value={this.state.date}
					/>
				</View>
				<View style={{position: 'absolute',backgroundColor: 'white', borderRadius: 10, width: 285, height:300, top: 100, borderColor: '#AAA', borderWidth:1}}>
					<TextInput
						style={{height: 300}}
						onChangeText={(temp) => {
							this.setState({article: temp});
							pushData('class/' + this.props.class + '/news/' + this.props.index, {
								title: this.state.title,
								date: this.state.date,
								article: temp
							});
						}}
						value={this.state.article}
						multiline={true}
					/>
				</View>
			</View>
		);
	}

	student() {
		return(
			<View style={{width:290, height: 480, position: 'absolute', left:40, top:56}}>
				<ScrollView style={{flex: 1}}>
				<Text style={{fontSize:30, color: 'black',marginBottom: 10}}>
					{this.state.title}
				</Text>
				<Text style={{fontSize:18, color: 'black',marginBottom: 10}}>
					{this.state.date}
				</Text>
				<Text style={{fontSize:20, color: 'black'}}>
					{this.state.article}
				</Text>
				</ScrollView>
			</View>
		);
	}

	render() {
		let { permissions } = this.state;
		return (
			<View style={{alignItems: 'center'}}>
				<Image style={{width:360, height: 592}} source={require('../../img/Back3.gif')} />
          {permissions == 1?
            this.student()
          :permissions == 0?
            this.teacher()
          :null}
				<TouchableOpacity onPress={() => home(this.props.navigator, this.props.user, this.props.permissions)} style={{position: 'absolute',left:15,top:0}}>
					<Image style={{width:92*0.8, height: 69*0.8}} source={require('../../img/house.png')} />
				</TouchableOpacity>
				<TouchableOpacity onPress={() => back(this.props.navigator)} style={{position: 'absolute',left:90,top:10}}>
					<Image style={{width:45*1.2, height:35*1.2}} source={require('../../img/gobback.png')} />
				</TouchableOpacity>
				<TouchableOpacity onPress={() => menu(this.props.navigator, this.props.user, this.props.permissions)} style={{position: 'absolute',right:25,top:7}}>
					<Image style={{width:138*0.8, height: 56*0.8}} source={require('../../img/menu3.png')} />
				</TouchableOpacity>
			</View>

		);
	}
}

const styles = StyleSheet.create({

});
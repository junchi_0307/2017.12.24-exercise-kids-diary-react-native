import React, { Component } from 'react';
import { Hoshi } from 'react-native-textinput-effects';
import {
	StyleSheet,
	View,
	TouchableOpacity,
	Image,
	Text,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

import { menu, home, back } from '../complex.js';
import { getData, pushData } from '../firebase.js';

export default class example extends Component {
	constructor(props) {
		super(props);
		this.props.navigator.toggleNavBar({
			to: 'hidden',
			animated: false,
		});
		this.addCalendar.bind(this);
		this.state = {
			name:'',
			location:'',
			date:''
		};
	}

	addCalendar() {
		let { date, location, name } = this.state;
		if(date != '' && date.length == 10 && date.indexOf('-') == 4 && date.lastIndexOf('-') == 7 && name != '' && location != ''){
			getData('class/' + this.props.class + '/calendar').then(json => {
				let calendar = json.val();
				calendar.push({date: date, text: name, address: location});
				pushData('class/' + this.props.class + '/calendar', calendar).then(() => {
					back(this.props.navigator);
				});
			});
		}
	}

	render() {
		return (
			<View style={{alignItems: 'center'}}>
				<Image style={{width:360, height: 592}} source={require('../../img/back6.png')} />
				<View style={{top:10,left:135,position: 'absolute'}}>
					<Text style={{fontSize:30, color: 'black'}}>
						行事曆
					</Text>
				</View>
				<TouchableOpacity onPress={() => home(this.props.navigator, this.props.user, this.props.permissions)} style={{position: 'absolute',left:10,top:0}}>
					<Image style={{width:92*0.7, height: 69*0.7}} source={require('../../img/house.png')} />
				</TouchableOpacity>
				<TouchableOpacity onPress={() => menu(this.props.navigator, this.props.user, this.props.permissions)} style={{position: 'absolute',right:3,top:7}}>
					<Image style={{width:138*0.7, height: 56*0.7}} source={require('../../img/menu3.png')} />
				</TouchableOpacity>
				<TouchableOpacity onPress={() => back(this.props.navigator)} style={{position: 'absolute',left:10,top:50}}>
					<Image style={{width:45, height:35}} source={require('../../img/gobback.png')} />
				</TouchableOpacity>
				<View style={{position: 'absolute',backgroundColor: 'white', borderRadius: 10, width: 300, height:190, top: 190, borderColor: '#AAA', borderWidth:1}}>
					<Hoshi label='事件名稱:' borderColor='#AAA' 
						onChangeText={(temp) => this.setState({name: temp})}
       					value={this.state.name}
       				/>
					<Hoshi label='地點:' borderColor='#AAA'
						onChangeText={(temp) => this.setState({location: temp})}
       					value={this.state.location}
					/>
					<Hoshi label='日期:' borderColor='#AAA'
						onChangeText={(temp) => this.setState({date: temp})}
       					value={this.state.date}
					/>
				</View>
				<TouchableOpacity onPress={() => this.addCalendar()} style={{position: 'absolute', width: 80, height: 80, top: 450}}>
					<Ionicons name='ios-checkmark-circle' size={80} color='#00FF00' />
				</TouchableOpacity>
			</View>
		);
	}
}

const styles = StyleSheet.create({

});
import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  Image,
  Text,
  TouchableOpacity,
  TextInput
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

import { getData, pushData } from '../firebase.js';
import { menu, home, back } from '../complex.js';
import { uploadFacePhotoList } from '../uploadPhoto.js';

export default class example extends Component {
  constructor(props) {
    super(props);
    this.props.navigator.toggleNavBar({
      to: 'hidden',
      animated: false,
    });
    this.logout.bind(this);
    this.menu.bind(this);
    this.student.bind(this);
    this.teacher.bind(this);
    this.addPersonalPhoto.bind(this);
    this.state = {
      permissions: -1
    };
  }

  logout(){
    if(this.props.permissions == 1){
      this.props.navigator.resetTo({
        screen: 'Mean'
      });
    }else if(this.props.permissions == 0){
      back(this.props.navigator);
    }
  }

  menu(){
    this.props.navigator.resetTo({
      screen: 'Menu',
      passProps: {
        user: this.props.user,
        permissions: this.props.permissions
      }
    });
  }

  componentDidMount() {
    getData('ip').then((json) => {
      this.setState({server: json.val()});
    });
    getData('webAPI').then((json) => {
      this.setState({webAPI: json.val()});
    });
    if(this.props.permissions == 1){
      getData('users/' + this.props.user).then((json) => {
        let data = json.val();
        let personalPhoto = [];
        let family = data.family;

        data.personalPhoto.forEach(photoData => {
          if(photoData != ''){
            personalPhoto.push(this.state.server + photoData);
          }else{
            personalPhoto.push(photoData);
          }
        });

        for(let i=family.length ; i<4 ; i++){
          family.push({name: '', phone: '', title: ''});
        }

        this.setState({
          no: data.no,
          name: data.name,
          birthday: data.birthday,
          address: data.address,
          height: data.height,
          weight: data.weight,
          health: data.health,
          drugAllergy: data.drugAllergy,
          family: family,
          personalPhoto: personalPhoto,
          permissions: this.props.permissions
        });

        console.log(this.state);
      });
    }else if(this.props.permissions == 0){
      getData('users/' + this.props.student).then((json) => {
        let data = json.val();
        let personalPhoto = [];
        let family = data.family;

        data.personalPhoto.forEach(photoData => {
          if(photoData != ''){
            personalPhoto.push(this.state.server + photoData);
          }else{
            personalPhoto.push(photoData);
          }
        });
        if(personalPhoto.length == 1 && personalPhoto[0] == ''){
          personalPhoto[0] = 'add';
        }else{
          personalPhoto.push('add');
        }

        for(let i=family.length ; i<4 ; i++){
          family.push({name: '', phone: '', title: ''});
        }

        this.setState({
          no: data.no,
          name: data.name,
          birthday: data.birthday,
          address: data.address,
          height: data.height,
          weight: data.weight,
          health: data.health,
          drugAllergy: data.drugAllergy,
          family: family,
          personalPhoto: personalPhoto,
          permissions: this.props.permissions
        });

        console.log(this.state);
      });
    }
  }

  addPersonalPhoto(index) {
    uploadFacePhotoList(
      this.state.server,
      '/Server/students/' + this.state.no,
      'users/' + this.props.student + '/personalPhoto',
      index,
      (uri) => {
        getData('users/' + this.props.student).then((json) => {
          let data = json.val();
          let personalPhoto = [];

          data.personalPhoto.forEach(photoData => {
            personalPhoto.push(this.state.server + photoData);
          });
          personalPhoto.push('add');

          this.setState({
            personalPhoto: personalPhoto
          });
        });

        fetch(this.state.webAPI + '/addFace', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            user: this.props.student,
            uri: this.state.server + uri
          })
        });
      }
    );
  }

  student(){
    return (
      <ScrollView>
        <Image style={{width:360, height: 1066}} source={require('../../img/back2.gif')} />
        <TouchableOpacity onPress={() => this.menu()} style={{position: 'absolute',right:7,top:10}}>
          <Image style={{width:138*0.7, height: 56*0.7}} source={require('../../img/menu3.png')} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.logout()} style={{position: 'absolute',left:7,top:10}}>
          <Image style={{width:45*1.3, height:35*1.3}} source={require('../../img/gobback.png')} />
        </TouchableOpacity>
        <View style={{position: 'absolute', width: 360, height: 1066, alignItems: 'center', top: 50}}>
          <Image source={{uri: this.state.personalPhoto[0]}} style={{height: 180, width: 180, borderRadius: 90, margin: 5, borderColor: '#AAA', borderWidth: 1}} />
          <View style={{backgroundColor: 'white', height: 50, width: 180, margin: 5, borderRadius: 30, borderColor: '#AAA', borderWidth: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Text style={{fontSize: 24, color: '#555'}}>{this.state.name}</Text>
          </View>
          <View style={{width: 360, height: 40, margin: 5, alignItems: 'center', justifyContent: 'center'}}>
            <Text style={{fontSize: 26, color: 'black'}}>基本資料</Text>
          </View>
          <View style={{width: 300, height: 250, backgroundColor: 'white', margin: 5, borderRadius: 30, borderColor: '#AAA', borderWidth: 1, flexDirection: 'row'}}>
            <View style={{flex: 1}}>
              <View style={styles.tableNoBorder}><Text style={styles.font}>生日</Text></View>
              <View style={styles.tableTopBorder}><Text style={styles.font}>地址</Text></View>
              <View style={styles.tableTopBorder}><Text style={styles.font}>身高/體重</Text></View>
              <View style={styles.tableTopBorder}><Text style={styles.font}>身體狀況</Text></View>
              <View style={styles.tableTopBorder}><Text style={styles.font}>藥物過敏</Text></View>
            </View>
            <View style={{flex: 2, borderLeftWidth: 1, borderColor: '#CCC'}}>
              <View style={styles.tableNoBorder}><Text style={styles.font}>{this.state.birthday}</Text></View>
              <View style={styles.tableTopBorder}><Text style={styles.font}>{this.state.address}</Text></View>
              <View style={styles.tableTopBorder}><Text style={styles.font}>{this.state.height}/{this.state.weight}</Text></View>
              <View style={styles.tableTopBorder}><Text style={styles.font}>{this.state.health}</Text></View>
              <View style={styles.tableTopBorder}><Text style={styles.font}>{this.state.drugAllergy}</Text></View>
            </View>
          </View>
          <View style={{width: 360, height: 40, margin: 5, alignItems: 'center', justifyContent: 'center'}}>
            <Text style={{fontSize: 26, color: 'black'}}>家庭成員</Text>
          </View>
          <View style={{width: 300, height: 250, backgroundColor: 'white', margin: 5, borderRadius: 30, borderColor: '#AAA', borderWidth: 1, flexDirection: 'row'}}>
            <View style={{flex: 2}}>
              <View style={styles.tableNoBorder}><Text style={styles.font}>姓名</Text></View>
              {this.state.family.map(data => {
                return(<View style={styles.tableTopBorder}><Text style={styles.font}>{data.name}</Text></View>);
              })}
            </View>
            <View style={{flex: 2, borderLeftWidth: 1, borderColor: '#CCC'}}>
              <View style={styles.tableNoBorder}><Text style={styles.font}>稱謂</Text></View>
              {this.state.family.map(data => {
                return(<View style={styles.tableTopBorder}><Text style={styles.font}>{data.title}</Text></View>);
              })}
            </View>
            <View style={{flex: 3, borderLeftWidth: 1, borderColor: '#CCC'}}>
              <View style={styles.tableNoBorder}><Text style={styles.font}>連絡電話</Text></View>
              {this.state.family.map(data => {
                return(<View style={styles.tableTopBorder}><Text style={styles.font}>{data.phone}</Text></View>);
              })}
            </View>
          </View>
          <ScrollView horizontal={true} style={{marginTop: 5, marginBottom: 5}}>
            <View style={{height: 100, width: this.state.personalPhoto.length * 90, justifyContent: 'center', flexDirection: 'row'}}>
              {this.state.personalPhoto.map((data, index) => {
                return(<Image key={index} style={{width:80, height: 80, borderRadius: 40, margin: 5}} source={{uri: data}} />)
              })}
            </View>
          </ScrollView>
        </View>
      </ScrollView>
    );
  }

  teacher(){
    return (
      <ScrollView>
        <Image style={{width:360, height: 1066}} source={require('../../img/back2.gif')} />
        <TouchableOpacity onPress={() => this.menu()} style={{position: 'absolute',right:7,top:10}}>
          <Image style={{width:138*0.7, height: 56*0.7}} source={require('../../img/menu3.png')} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.logout()} style={{position: 'absolute',left:7,top:10}}>
          <Image style={{width:45*1.3, height:35*1.3}} source={require('../../img/gobback.png')} />
        </TouchableOpacity>
        <View style={{position: 'absolute', width: 360, height: 1066, alignItems: 'center', top: 50}}>
          <Image source={{uri: this.state.personalPhoto[0]}} style={{height: 180, width: 180, borderRadius: 90, margin: 5, borderColor: '#AAA', borderWidth: 1}} />
          <View style={{backgroundColor: 'white', height: 50, width: 180, margin: 5, borderRadius: 30, borderColor: '#AAA', borderWidth: 1, alignItems: 'center', justifyContent: 'center'}}>
            <TextInput
              style={{fontSize: 24, color: '#555', height: 50, width: 140}}
              value={this.state.name}
              onChangeText={name => {
                this.setState({name});
                pushData('users/' + this.props.student, {name: name});
            }} />
          </View>
          <View style={{width: 360, height: 40, margin: 5, alignItems: 'center', justifyContent: 'center'}}>
            <Text style={{fontSize: 26, color: 'black'}}>基本資料</Text>
          </View>
          <View style={{width: 300, height: 250, backgroundColor: 'white', margin: 5, borderRadius: 30, borderColor: '#AAA', borderWidth: 1, flexDirection: 'row'}}>
            <View style={{flex: 1}}>
              <View style={styles.tableNoBorder}><Text style={styles.font}>生日</Text></View>
              <View style={styles.tableTopBorder}><Text style={styles.font}>地址</Text></View>
              <View style={styles.tableTopBorder}><Text style={styles.font}>身高/體重</Text></View>
              <View style={styles.tableTopBorder}><Text style={styles.font}>身體狀況</Text></View>
              <View style={styles.tableTopBorder}><Text style={styles.font}>藥物過敏</Text></View>
            </View>
            <View style={{flex: 2, borderLeftWidth: 1, borderColor: '#CCC'}}>
              <View style={styles.tableNoBorder}>
                <TextInput
                  style={[styles.font, {height: 50, width: 140}]}
                  value={this.state.birthday}
                  onChangeText={birthday => {
                    this.setState({birthday});
                    pushData('users/' + this.props.student, {birthday: birthday});
                }} />
              </View>
              <View style={styles.tableTopBorder}>
                <TextInput
                  style={[styles.font, {height: 50, width: 140}]}
                  value={this.state.address}
                  onChangeText={address => {
                    this.setState({address});
                    pushData('users/' + this.props.student, {address: address});
                }} />
              </View>
              <View style={[styles.tableTopBorder, {flexDirection: 'row'}]}>
                <TextInput
                  style={[styles.font, {height: 50, width: 60}]}
                  value={this.state.height}
                  onChangeText={height => {
                    this.setState({height});
                    pushData('users/' + this.props.student, {height: height});
                }} />
                <Text style={styles.font}>/</Text>
                <TextInput
                  style={[styles.font, {height: 50, width: 60}]}
                  value={this.state.weight}
                  onChangeText={weight => {
                    this.setState({weight});
                    pushData('users/' + this.props.student, {weight: weight});
                }} />
              </View>
              <View style={styles.tableTopBorder}>
                <TextInput
                  style={[styles.font, {height: 50, width: 140}]}
                  value={this.state.health}
                  onChangeText={health => {
                    this.setState({health});
                    pushData('users/' + this.props.student, {health: health});
                }} />
              </View>
              <View style={styles.tableTopBorder}>
                <TextInput
                  style={[styles.font, {height: 50, width: 140}]}
                  value={this.state.drugAllergy}
                  onChangeText={drugAllergy => {
                    this.setState({drugAllergy});
                    pushData('users/' + this.props.student, {drugAllergy: drugAllergy});
                }} />
                </View>
            </View>
          </View>
          <View style={{width: 360, height: 40, margin: 5, alignItems: 'center', justifyContent: 'center'}}>
            <Text style={{fontSize: 26, color: 'black'}}>家庭成員</Text>
          </View>
          <View style={{width: 300, height: 250, backgroundColor: 'white', margin: 5, borderRadius: 30, borderColor: '#AAA', borderWidth: 1, flexDirection: 'row'}}>
            <View style={{flex: 2}}>
              <View style={styles.tableNoBorder}><Text style={styles.font}>姓名</Text></View>
              {this.state.family.map((data, index) => {
                return(
                  <View style={styles.tableTopBorder} key={index}>
                    <TextInput
                      style={[styles.font, {height: 50, width: 80}]}
                      value={data.name}
                      onChangeText={name => {
                        let { family } = this.state;
                        family[index]['name'] = name;
                        this.setState({family});
                        pushData('users/' + this.props.student, {family: family});
                    }} />
                  </View>
                );
              })}
            </View>
            <View style={{flex: 2, borderLeftWidth: 1, borderColor: '#CCC'}}>
              <View style={styles.tableNoBorder}><Text style={styles.font}>稱謂</Text></View>
              {this.state.family.map((data, index) => {
                return(
                  <View style={styles.tableTopBorder} key={index}>
                    <TextInput
                      style={[styles.font, {height: 50, width: 90}]}
                      value={data.title}
                      onChangeText={title => {
                        let { family } = this.state;
                        family[index]['title'] = title;
                        this.setState({family});
                        pushData('users/' + this.props.student, {family: family});
                    }} />
                  </View>
                );
              })}
            </View>
            <View style={{flex: 3, borderLeftWidth: 1, borderColor: '#CCC'}}>
              <View style={styles.tableNoBorder}><Text style={styles.font}>連絡電話</Text></View>
              {this.state.family.map((data, index) => {
                return(
                  <View style={styles.tableTopBorder} key={index}>
                    <TextInput
                      style={[styles.font, {height: 50, width: 120}]}
                      value={data.phone}
                      onChangeText={phone => {
                        let { family } = this.state;
                        family[index]['phone'] = phone;
                        this.setState({family});
                        pushData('users/' + this.props.student, {family: family});
                    }} />
                  </View>
                );
              })}
            </View>
          </View>
          <ScrollView horizontal={true} style={{marginTop: 5, marginBottom: 5}}>
            <View style={{height: 100, width: this.state.personalPhoto.length * 90, justifyContent: 'center', flexDirection: 'row'}}>
              {this.state.personalPhoto.map((data, index) => {
                if(data == 'add'){
                  return(
                    <TouchableOpacity key={index} onPress={() => this.addPersonalPhoto(index)} style={{margin: 5}}>
                      <View style={{height: 80, width: 80, borderRadius: 40, backgroundColor: 'white', opacity: 0.7, alignItems: 'center', justifyContent: 'center'}}>
                        <Ionicons name='ios-add' size={60} color='black' />
                      </View>
                    </TouchableOpacity>
                  )
                }else{
                  return(<Image key={index} style={{width:80, height: 80, borderRadius: 40, margin: 5}} source={{uri: data}} />)
                }
              })}
            </View>
          </ScrollView>
        </View>
      </ScrollView>
    );
  }

  render() {
    console.log(this.state.permissions);
    let { permissions } = this.state;
    return (
      <View>
        {permissions == 1?
          this.student()
        :permissions == 0?
          this.teacher()
        :null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  tableNoBorder:{
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  tableTopBorder:{
    flex: 1,
    borderTopWidth: 1,
    borderColor: '#CCC',
    alignItems: 'center',
    justifyContent: 'center'
  },
  font: {
    fontSize: 18
  }
});
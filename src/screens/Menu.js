import React, { Component } from 'react';
import {
	StyleSheet,
	View,
	Image,
	Text,
	TouchableOpacity,
} from 'react-native';

import { getData } from '../firebase.js';
import { home } from '../complex.js';

export default class example extends Component {
	constructor(props) {
		super(props);
    this.props.navigator.toggleNavBar({
      to: 'hidden',
      animated: false,
    });
    this.class.bind(this);
    this.menu2.bind(this);
    this.board.bind(this);
    this.calendar.bind(this);
		this.state = {

		};
	}

	componentDidMount() {
    getData('ip').then((json) => {
      this.setState({server: json.val()});
    });
    getData('users/' + this.props.user).then((json) => {
	    let data = json.val();

      this.setState({
        permissions: this.props.permissions,
        class: data.class
      });
    });
	}

  class(){
    this.props.navigator.push({
      screen: 'Class',
      passProps: {
        user: this.props.user,
        permissions: this.props.permissions,
        class: this.state.class
      }
    });
  }

  menu2(){
    this.props.navigator.push({
      screen: 'Menu2',
      passProps: {
        user: this.props.user,
        permissions: this.props.permissions
      }
    });
  }

  board(){
    this.props.navigator.push({
      screen: 'Board',
      passProps: {
        user: this.props.user,
        permissions: this.props.permissions,
        class: this.state.class
      }
    });
  }

  calendar(){
    this.props.navigator.push({
      screen: 'Calendar',
      passProps: {
        user: this.props.user,
        permissions: this.props.permissions,
        class: this.state.class
      }
    });
  }

	render() {
		return (
			<View style={{alignItems: 'center'}}>
				<Image style={{width:360, height: 592}} source={require('../../img/menu1.png')} />
				<TouchableOpacity onPress={() => home(this.props.navigator, this.props.user, this.props.permissions)} style={{position: 'absolute',left:15}}>
					<Image style={{width:92*0.8, height: 69*0.8}} source={require('../../img/house.png')} />
				</TouchableOpacity>
				<TouchableOpacity onPress={() => this.class()} style={{position: 'absolute',top:100}}>
					<View style={{width:240, height:80, backgroundColor: 'white',alignItems: 'center', justifyContent: 'center', borderRadius: 50}}>
						<Text style={{fontSize: 36}}>
							班級
						</Text>
					</View>
				</TouchableOpacity>
				<TouchableOpacity style={{position: 'absolute',right:15,top:10}}>
					<Image style={{width:138*0.8, height: 56*0.8}} source={require('../../img/menu3.png')} />
				</TouchableOpacity>
				<TouchableOpacity onPress={() => this.menu2()} style={{position: 'absolute',top:200}}>
					<View style={{width:240, height:80, backgroundColor: 'white',alignItems: 'center', justifyContent: 'center', borderRadius: 50}}>
						<Text style={{fontSize: 36}}>
							學習紀錄
						</Text>
					</View>
				</TouchableOpacity>
				<TouchableOpacity onPress={() => this.board()} style={{position: 'absolute',top:300}}>
					<View style={{width:240, height:80, backgroundColor: 'white',alignItems: 'center', justifyContent: 'center', borderRadius: 50}}>
						<Text style={{fontSize: 36}}>
							公告欄
						</Text>
					</View>
				</TouchableOpacity>
					<TouchableOpacity onPress={() => this.calendar()} style={{position: 'absolute',top:400}}>
					<View style={{width:240, height:80, backgroundColor: 'white',alignItems: 'center', justifyContent: 'center', borderRadius: 50}}>
						<Text style={{fontSize: 36}}>
							行事曆
						</Text>
					</View>
				</TouchableOpacity>

			</View>

		);
	}
}
import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/EvilIcons'
import {
  StyleSheet,
  View,
  ScrollView,
  Image,
  Text,
  TouchableOpacity,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

import { getData, pushData } from '../firebase.js';
import { uploadPhoto } from '../uploadPhoto.js';
import { menu, back, home } from '../complex.js';

export default class example extends Component {
  constructor(props) {
    super(props);
    this.props.navigator.toggleNavBar({
      to: 'hidden',
      animated: false,
    });
    this.student.bind(this);
    this.teacher.bind(this);
    this.uploadPhoto.bind(this);
    this.uploadEvaluationPhoto.bind(this);
    this.state = {
    };
  }

  uploadPhoto(serverImageName, callback){
    uploadPhoto(
      this.state.server,
      '/Server/classroom/' + this.state.className,
      'class/' + this.state.className,
      serverImageName,
      () => {
        getData('class/' + this.props.class).then((json) => {
          let data = json.val();

          callback(data);
        });
      }
    );
  }

  uploadEvaluationPhoto(index, callback){
    uploadPhoto(
      this.state.server,
      '/Server/classroom/' + this.state.className,
      'class/' + this.state.className + '/evaluation',
      index,
      () => {
        getData('class/' + this.props.class).then((json) => {
          let data = json.val();
          let { evaluation } = data;

          for(let i =0; i< evaluation.length; i++){
            evaluation[i] = this.state.server + evaluation[i];
          }
          for(let i=evaluation.length ; i<2 ; i++){
            evaluation.push('');
          }
          let height = 800 + 245 * evaluation.length;

          this.setState({
            evaluation: evaluation,
            height: height
          })
        });
      }
    );
  }

  student() {
    return(
      <View style={{top: 80, left: 0, width: 360, height: this.state.height, position: 'absolute', alignItems: 'center'}}>
        <Text style={{fontSize:30, color: 'black', margin: 10, marginTop: 25}}>
          主題網
        </Text>
        <Image style={{width:2492*0.12, height:1875*0.12, margin: 10}} source={{ uri: this.state.themeWebsite }} />
        <Text style={{fontSize:30, color: 'black', margin: 10, marginTop: 25}}>
          學習指標
        </Text>
        <Image style={{width:2492*0.12, height:1875*0.12, margin: 10}} source={{ uri: this.state.learn }} />
        <Text style={{fontSize:30, color: 'black', margin: 10, marginTop: 25, marginTop: 120}}>
          個人化課程評量
        </Text>
        {this.state.evaluation.map((data, index) => {
          return(<Image key={index} style={{width:2492*0.12, height:1875*0.12, margin: 10}} source={{ uri: data }} />)
        })}
      </View>
    );
  }

  teacher() {
    return(
      <View style={{top: 80, left: 0, width: 360, height: this.state.height, position: 'absolute', alignItems: 'center'}}>
        <Text style={{fontSize:30, color: 'black', margin: 10, marginTop: 25}}>
          主題網
        </Text>
        <TouchableOpacity
          onPress={() => {
            this.uploadPhoto('themeWebsite', (data) => {
              this.setState({
                themeWebsite: data.themeWebsite=='' ?'' :this.state.server + data.themeWebsite
              })
            })
          }}
        >
          <View style={{width:2492*0.12, height:1875*0.12, margin: 10, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center'}}>
            {this.state.themeWebsite != ''?
              <Image source={{uri: this.state.themeWebsite}} style={{width:2492*0.12, height:1875*0.12}} />
            :
              <Ionicons name='ios-add' size={60} color='#999' />
            }
          </View>
        </TouchableOpacity>
        <Text style={{fontSize:30, color: 'black', margin: 10, marginTop: 25}}>
          學習指標
        </Text>
        <TouchableOpacity
          onPress={() => {
            this.uploadPhoto('learn', (data) => {
              this.setState({
                learn: data.learn=='' ?'' :this.state.server + data.learn
              })
            })
          }}
        >
          <View style={{width:2492*0.12, height:1875*0.12, margin: 10, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center'}}>
            {this.state.learn != ''?
              <Image source={{uri: this.state.learn}} style={{width:2492*0.12, height:1875*0.12}} />
            :
              <Ionicons name='ios-add' size={60} color='#999' />
            }
          </View>
        </TouchableOpacity>
        <Text style={{fontSize:30, color: 'black', margin: 10, marginTop: 25, marginTop: 120}}>
          個人化課程評量
        </Text>
        {this.state.evaluation.map((data, index) => {
          return(
            <TouchableOpacity
              onPress={() => {
                this.uploadEvaluationPhoto(index)
              }}
              key={index}
            >
              <View style={{width:2492*0.12, height:1875*0.12, margin: 10, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center'}}>
                {data != ''?
                  <Image source={{uri: data}} style={{width:2492*0.12, height:1875*0.12}} />
                :
                  <Ionicons name='ios-add' size={60} color='#999' />
                }
              </View>
            </TouchableOpacity>
          )
        })}
      </View>
    );
  }

  componentDidMount() {
    getData('ip').then((json) => {
      this.setState({server: json.val()});
    });
    getData('class/' + this.props.class).then((json) => {
      let data = json.val();
      let { evaluation } = data;

      for(let i =0; i< evaluation.length; i++){
        if(evaluation[i] != ''){
          evaluation[i] = this.state.server + evaluation[i];
        }
      }
      if(this.props.permissions == 0){
        evaluation.push('');
        for(let i=evaluation.length ; i<2 ; i++){
          evaluation.push('');
        }
      }
      let height = 910 + 245 * evaluation.length;

      this.setState({
        permissions: this.props.permissions,
        themeWebsite: data.themeWebsite=='' ?'' :this.state.server + data.themeWebsite,
        learn: data.learn=='' ?'' :this.state.server + data.learn,
        evaluation: evaluation,
        className: data.className,
        height: height
      });
    });
  }

  render() {
    let { permissions } = this.state;
    return (
      <ScrollView>
        <View style={{width: 360, height: this.state.height, backgroundColor: '#fcebeb'}}>
        <Image style={{width:360, height: 115}} source={require('../../img/back5.png')} />
        <TouchableOpacity onPress={() => home(this.props.navigator, this.props.user, this.props.permissions)} style={{position: 'absolute',left:10,top:0}}>
          <Image style={{width:92*0.7, height: 69*0.7}} source={require('../../img/house.png')} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => menu(this.props.navigator, this.props.user, this.props.permissions)} style={{position: 'absolute',right:3,top:7}}>
          <Image style={{width:138*0.7, height: 56*0.7}} source={require('../../img/menu3.png')} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => back(this.props.navigator)} style={{position: 'absolute',left:10,top:50}}>
          <Image style={{width:45, height:35}} source={require('../../img/gobback.png')} />
        </TouchableOpacity>
          {permissions == 1?
            this.student()
          :permissions == 0?
            this.teacher()
          :null}
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({

});
import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  Image,
  Text,
  TouchableOpacity,
  TextInput
} from 'react-native';

import { getData, pushData } from '../firebase.js';
import { uploadPhoto } from '../uploadPhoto.js';
import { menu, back, home } from '../complex.js';

export default class example extends Component {
  constructor(props) {
    super(props);
    this.props.navigator.toggleNavBar({
      to: 'hidden',
      animated: false,
    });
    this.choosePhoto.bind(this);
    this.student.bind(this);
    this.teacher.bind(this);
    this.state = {
      permissions: -1,
      teacher: []
    };
  }

  componentDidMount() {
    getData('ip').then((json) => {
      this.setState({server: json.val()});
    });
    for(let i=0 ; i<this.props.teacher.length ; i++){
      getData('users/' + this.props.teacher[i]).then((json) => {
        let {teacher} = this.state;
        let data = json.val();
        teacher.push({
          name: data.name,
          phone: data.phone,
          email: data.email,
          education: data.education,
          photo: this.state.server + data.photo,
          no: data.no,
          user: data.user
        });
        this.setState({
          teacher: teacher
        });
        if(i == this.props.teacher.length-1){
          this.setState({
            permissions: this.props.permissions
          });
        }
      });
    }
  }

  choosePhoto(teacherData){
    console.log(teacherData);
    console.log('/Server/teacher/' + teacherData.no);
    console.log('class/' + teacherData.user);
    uploadPhoto(
      this.state.server,
      '/Server/teacher/' + teacherData.no,
      'users/' + teacherData.user,
      'photo',
      () => {
        this.setState({
          teacher: []
        });
        for(let i=0 ; i<this.props.teacher.length ; i++){
          getData('users/' + this.props.teacher[i]).then((json) => {
            let {teacher} = this.state;
            let data = json.val();
            teacher.push({
              name: data.name,
              phone: data.phone,
              email: data.email,
              education: data.education,
              photo: this.state.server + data.photo,
              no: data.no,
              user: data.user
            });
            this.setState({
              teacher: teacher
            });
          });
        }
      }
    );
  }

  student(teacherData, index) {
    return (
      <View key={index} style={{width: 360, height: 450, alignItems: 'center', justifyContent: 'center'}}>
        <Image source={{uri: teacherData.photo}} style={{margin: 5, height: 150, width: 150, borderRadius: 75}}/>  
        <View style={{ width: 240, height: 50, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
          <Text style={{fontSize: 20, flex: 1}}>老師：{teacherData.name}</Text>
        </View>
        <View style={{ width: 240, height: 50, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
          <Text style={{fontSize: 20, flex: 1}}>電話：{teacherData.phone}</Text>
        </View>
        <View style={{ width: 240, height: 50, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
          <Text style={{fontSize: 20, flex: 1}}>E-mail：{teacherData.email}</Text>
        </View>
        <View style={{ width: 240, height: 50, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
          <Text style={{fontSize: 20, flex: 1}}>學歷：{teacherData.education}</Text>
        </View>
      </View>
    );
  }

  teacher(teacherData, index){
    return (
      <View key={index} style={{width: 360, height: 450, alignItems: 'center', justifyContent: 'center'}}>
        <TouchableOpacity onPress={() => this.choosePhoto(teacherData)} style={{margin: 5, borderRadius: 75}}>
          <Image source={{uri: teacherData.photo}} style={{height: 150, width: 150, borderRadius: 75}}/>          
        </TouchableOpacity>
        <View style={{ width: 240, height: 50, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
          <Text style={{fontSize: 20, flex: 1}}>老師：</Text>
          <TextInput
            style={{
              flex: 2,
              fontSize: 20,
              marginTop: 2
            }}
            value={teacherData.name}
            onChangeText={text => {
              let {teacher} = this.state;
              teacher[index].name = text;
              this.setState({ teacher });
              pushData('users/' + teacherData.user, {name: text});
            }}
          />
        </View>
        <View style={{ width: 240, height: 50, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
          <Text style={{fontSize: 20, flex: 1}}>電話：</Text>
          <TextInput
            style={{
              flex: 2,
              fontSize: 20,
              marginTop: 2
            }}
            value={teacherData.phone}
            onChangeText={text => {
              let {teacher} = this.state;
              teacher[index].phone = text;
              this.setState({ teacher });
              pushData('users/' + teacherData.user, {phone: text});
            }}
          />
        </View>
        <View style={{ width: 240, height: 50, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
          <Text style={{fontSize: 20, flex: 1}}>E-mail：</Text>
          <TextInput
            style={{
              flex: 2,
              fontSize: 20,
              marginTop: 2
            }}
            value={teacherData.email}
            onChangeText={text => {
              let {teacher} = this.state;
              teacher[index].email = text;
              this.setState({ teacher });
              pushData('users/' + teacherData.user, {email: text});
            }}
          />
        </View>
        <View style={{ width: 240, height: 50, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
          <Text style={{fontSize: 20, flex: 1}}>學歷：</Text>
          <TextInput
            style={{
              flex: 2,
              fontSize: 20,
              marginTop: 2
            }}
            value={teacherData.education}
            onChangeText={text => {
              let {teacher} = this.state;
              teacher[index].education = text;
              this.setState({ teacher });
              pushData('users/' + teacherData.user, {education: text});
            }}
          />
        </View>
      </View>
    );
  }

  render() {
    let { permissions } = this.state;
    console.log(this.state);
    return (
      <View>
        <ScrollView>
        <Image style={{width:360, height: 1066}} source={require('../../img/back4.png')} />
        <TouchableOpacity onPress={() => home(this.props.navigator, this.props.user, this.props.permissions)} style={{position: 'absolute',left:10,top:0}}>
          <Image style={{width:92*0.7, height: 69*0.7}} source={require('../../img/house.png')} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => menu(this.props.navigator, this.props.user, this.props.permissions)} style={{position: 'absolute',right:3,top:7}}>
          <Image style={{width:138*0.7, height: 56*0.7}} source={require('../../img/menu3.png')} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => back(this.props.navigator)} style={{position: 'absolute',left:10,top:50}}>
          <Image style={{width:45, height:35}} source={require('../../img/gobback.png')} />
        </TouchableOpacity>
        <View style={{top: 50, position: 'absolute', alignItems: 'center', justifyContent: 'center'}}>
          {permissions == 1?
            this.state.teacher.map((teacherData, index) => this.student(teacherData, index))
          :permissions == 0?
            this.state.teacher.map((teacherData, index) => this.teacher(teacherData, index))
          :null}
        </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({

});
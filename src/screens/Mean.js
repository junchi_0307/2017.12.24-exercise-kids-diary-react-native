import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  Image,
  TouchableOpacity
} from 'react-native';

import Svg, { Path } from 'react-native-svg';
import { Hoshi } from 'react-native-textinput-effects';
import PasswordInputText from 'react-native-hide-show-password-input';

import { getData } from '../firebase.js';

let {height, width} = Dimensions.get('window');
console.log('height:' + height);
console.log('width:' + width);

export default class example extends Component {
	constructor(props) {
		super(props);
		this.props.navigator.toggleNavBar({
			to: 'hidden',
			animated: false
		});
		this.login.bind(this);
		this.state = {
			user: '',
			userCheck: '',
			pass: '',
			permissions: '',
			userData: ''
		};
	}

	login(getData){
		console.log('user: ' + this.state.user);
		console.log('pass: ' + this.state.pass);
		getData('users').then((json) => {
			if(json.val()[this.state.user] == null){
				console.log("NOT USER");
				this.setState({user: '', pass: ''});
			}else{
				if(json.val()[this.state.user].password == this.state.pass){
					console.log("login success");
					this.setState({permissions: json.val()[this.state.user].permissions});
					this.setState({userData: json.val()[this.state.user]});
					console.log(this.state.permissions);
					console.log(this.state.userData);
					if(this.state.permissions == 1){
						this.props.navigator.resetTo({
							screen: 'Home',
							passProps: {
								user: this.state.user,
								permissions: json.val()[this.state.user].permissions
							}
						});
					}else{
						this.props.navigator.resetTo({
							screen: 'Students',
							passProps: {
								user: this.state.user,
								permissions: json.val()[this.state.user].permissions
							}
						});
					}
					this.setState({userCheck: this.state.user});
				}else{
					console.log("login error");
					this.setState({user: '', pass: ''});
				}
			}
		});
	}

	componentDidMount() {
  }

	render() {
		return (
			<View style={{flex: 1, backgroundColor: '#F5FAEE', alignItems: 'center'}}>
				<Svg height='200' width='1280' style={{position: 'absolute', top: 0, left: 0}}>
					<Path
						d='M1281.992,58.606c-17.384,18.847-45.011,17.207-60.299-4.203
							c-4.594-6.432-6.085-7.098-11.236-0.26c-18.113,24.029-47.861,23.109-65.709-1.347c-1.334-1.825-2.681-3.643-4.126-5.608
							c-25.671,37.354-60.056,27.415-76.048,0.286c-24.189,35.353-57.479,29.072-75.798,0.367c-26.519,36.603-58.872,26.969-76.175-0.926
							c-4.457,5.951-8.313,12.153-14.251,16.501c-18.929,13.852-42.939,9.704-56.883-10.234c-3.008-4.302-4.195-6.992-8.782-0.953
							c-21.175,27.9-51.742,25.678-71.512-5.874c-8.663,14.301-20.192,24.875-37.185,25.21c-17.248,0.339-29.425-9.182-38.491-25.416
							c-9.204,14.922-20.969,25.659-38.38,25.383c-17.108-0.274-29.229-9.867-37.146-25.085c-23.328,36.183-59.265,30.274-76.63,0.128
							c-8.495,14.559-20.226,24.68-37.139,24.921c-17.229,0.245-29.305-9.443-38.301-25.211c-9.146,15.016-21.084,25.369-38.438,25.257
							c-17.1-0.109-29.152-9.832-37.564-24.872c-2.252,3.135-3.908,5.577-5.698,7.912c-17.354,22.629-47.843,22.542-64.713-0.508
							c-3.829-5.232-5.217-8.096-10.396-0.704c-15.668,22.377-49.147,28.574-70.916-6.765c-8.768,14.182-20.286,24.796-37.428,24.888
							c-17.195,0.089-29.369-9.45-38.239-25.298c-9.18,14.909-20.941,25.54-38.543,25.342c-17.104-0.196-29.011-9.843-37.393-24.96
							c-2.413,3.297-4.287,6.022-6.324,8.617C54.99,77.183,24.823,76.765,7.982,54.475c-2.048-2.709-2.414-8.352-8.286-6.138
							c0-14.356,0.217-28.717-0.132-43.064c-0.105-4.318,0.829-5.905,5.148-5.05c1.285,0.254,2.663,0.041,3.998,0.041
							c424.319,0,848.636,0,1272.954,0C1283.479,19.701,1282.862,39.155,1281.992,58.606z'
						fill='#1D6886'
						stroke='none'
						scale={width/1280}
					/>
				</Svg>
				<Image style={{width: width/1.5, height: 790/830*width/1.5, top: 80}} source={require('../../img/back.png')} />
				<View style={{backgroundColor: '#EA7576', width: width, height: 70, bottom: 0, left: 0, position: 'absolute'}}></View>
				<View style={{backgroundColor: 'white', borderRadius: 20, width: 260, height: 140, top: 100, borderColor: '#AAA', borderWidth: 1}}>
					<View style={{width: 240, height: 140, top: 0, position: 'absolute'}}>
						<Hoshi label='user' borderColor='white' value={this.state.user} onChangeText={(user) => this.setState({user})} />
					</View>
					<View style={{width: 240, height: 140, top: 50, position: 'absolute', left: 10}}>
						<PasswordInputText value={this.state.pass} onChangeText={pass => this.setState({pass})} />
					</View>
				</View>
				<View style={{top: 120, flexDirection: 'row' , justifyContent: 'center' , width: width*0.75}}>
					<TouchableOpacity onPress={() => this.login(getData)}>
						<View style={{backgroundColor: '#FFE97F', width: 120, height: 60, borderRadius: 25, alignItems: 'center', justifyContent: 'center', borderColor: '#AAA', borderWidth: 1.5}}>
							<Text style={{fontSize: 20, color: 'black'}}>
								登入
							</Text>
						</View>
					</TouchableOpacity>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
});
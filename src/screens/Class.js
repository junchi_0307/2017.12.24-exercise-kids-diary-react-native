import React, { Component } from 'react';
import {
	StyleSheet,
	View,
	Image,
	Text,
	TouchableOpacity,
	TextInput,
} from 'react-native';

import { getData, pushData } from '../firebase.js';
import { menu, home } from '../complex.js';
import { uploadPhoto } from '../uploadPhoto.js';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import randomatic from 'randomatic';

export default class example extends Component {
	constructor(props) {
		super(props);
		this.props.navigator.toggleNavBar({
			to: 'hidden',
			animated: false,
		});
		this.student.bind(this);
		this.teacher.bind(this);
		this.onChangeIntroduction.bind(this);
		this.uploadPhoto.bind(this);
		this.ourteacher.bind(this);
		this.course.bind(this);
		this.state = {
			permissions: -1  //teacher: 0, students: 1
		};
	}

	uploadPhoto(){
		uploadPhoto(
			this.state.server,
			'/Server/classroom/' + this.state.className,
			'class/' + this.state.className,
			'classImage',
			() => {
				getData('class/' + this.props.class).then((json) => {
			    let data = json.val();

		      this.setState({
		        classImage: data.classImage=='' ?'' :this.state.server + data.classImage
		      });
		    });
			}
		);
	}

  ourteacher(){
    this.props.navigator.push({
      screen: 'Ourteacher',
      passProps: {
        user: this.props.user,
        permissions: this.props.permissions,
        teacher: this.state.teacher
      }
    });
  }

  course(){
    this.props.navigator.push({
      screen: 'Course',
      passProps: {
        user: this.props.user,
        permissions: this.props.permissions,
        class: this.props.class
      }
    });
  }

	onChangeIntroduction() {
	}

	student() {
		return (
			<View style={{width: 360, height: 482, top: 110, position: 'absolute', alignItems: 'center'}}>
				<Text style={{fontSize: 26, borderBottomWidth: 0.5, margin: 5}}>{this.state.name}</Text>
					<View style={{borderRadius: 15, borderWidth: 0.5, width: 260, height: 160, margin: 5, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center'}}>
						<Image source={{uri: this.state.classImage}} style={{borderRadius: 15, width: 260, height: 160}} />
					</View>
				<Text style={{fontSize: 26, borderBottomWidth: 0.5, margin: 5}}>簡 介</Text>
				<Text style={{height: 160, width: 260, fontSize: 16}}>
					{this.state.introduction}
				</Text>
			</View>
		);
	}

	teacher() {
		return (
			<View style={{width: 360, height: 482, top: 110, position: 'absolute', alignItems: 'center'}}>
				<TextInput
					style={{fontSize: 26, margin: 5, width: 150}}
					value={this.state.name}
					onChangeText={name=> {
						this.setState({name});
            pushData('class/' + this.state.className, {name: name});
					}}
				/>
				<TouchableOpacity onPress={() => this.uploadPhoto()} style={{borderRadius: 15, margin: 5}}>
					<View style={{borderRadius: 15, borderWidth: 0.5, width: 260, height: 160, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center'}}>
						{this.state.classImage != ''?
							<Image source={{uri: this.state.classImage}} style={{borderRadius: 15, width: 260, height: 160}} />
						:
							<Ionicons name='ios-add' size={60} color='#999' />
						}
					</View>
				</TouchableOpacity>
				<Text style={{fontSize: 26, borderBottomWidth: 0.5, margin: 5}}>簡 介</Text>
				<TextInput
					style={{height: 160, width: 260, fontSize: 16}}
					value={this.state.introduction}
					onChangeText={introduction => {
						this.setState({introduction});
						pushData('class/' + this.state.className, { introduction: introduction});
					}}
					multiline={true}
				/>
			</View>
		);
	}

	componentDidMount() {
    getData('ip').then((json) => {
      this.setState({server: json.val()});
    });
    getData('class/' + this.props.class).then((json) => {
	    let data = json.val();

      this.setState({
        permissions: this.props.permissions,
        classImage: data.classImage=='' ?'' :this.state.server + data.classImage,
        introduction: data.introduction,
        className: data.className,
        name: data.name,
        teacher: data.teacher
      });
    });
	}

	render() {
    let { permissions } = this.state;
    return (
			<View style={{alignItems: 'center'}}>
				<Image style={{width:360, height: 592}} source={require('../../img/Back3.gif')} />
				<TouchableOpacity onPress={() => home(this.props.navigator, this.props.user, this.props.permissions)} style={{position: 'absolute',left:15,top:0}}>
					<Image style={{width:92*0.8, height: 69*0.8}} source={require('../../img/house.png')} />
				</TouchableOpacity>
				<TouchableOpacity onPress={() => menu(this.props.navigator, this.props.user, this.props.permissions)} style={{position: 'absolute',right:25,top:7}}>
					<Image style={{width:138*0.8, height: 56*0.8}} source={require('../../img/menu3.png')} />
				</TouchableOpacity>
				<TouchableOpacity onPress={() => this.ourteacher()} style={{position: 'absolute',top:60,left:40}}>
					<View style={{width:160, height:50, backgroundColor: '#F1CD94',alignItems: 'center', justifyContent: 'center', borderRadius: 50}}>
						<Text style={{fontSize: 25}}>
							我們的老師
						</Text>
					</View>
				</TouchableOpacity>
				<TouchableOpacity onPress={() => this.course()} style={{position: 'absolute',top:60,right:40}}>
					<View style={{width:115, height:50, backgroundColor: '#F1CD94',alignItems: 'center', justifyContent: 'center', borderRadius: 50}}>
						<Text style={{fontSize: 25}}>
							課程
						</Text>
					</View>
				</TouchableOpacity>
        {permissions == 1?
          this.student()
        :permissions == 0?
          this.teacher()
        :null}
			</View>
    );
	}
}
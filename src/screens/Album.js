import React, { Component } from 'react';
import {
	StyleSheet,
	View,
	TouchableOpacity,
	Image,
	Text,
	ScrollView
} from 'react-native';
import GridList from 'react-native-grid-list';
import Ionicons from 'react-native-vector-icons/Ionicons';

import { getData } from '../firebase.js';
import { menu, home, back } from '../complex.js';

export default class example extends Component {
	constructor(props) {
		super(props);
		this.props.navigator.toggleNavBar({
			to: 'hidden',
			animated: false,
		});
		this.photos.bind(this);
		this.updatePhoto.bind(this);
		this.state = {
			photos: [],
			permissions: -1
		};
	}

	componentDidMount() {
    getData('ip').then((json) => {
      this.setState({server: json.val()});
    });
    getData('webAPI').then((json) => {
      this.setState({webAPI: json.val()});
    });

    if(this.props.permissions == 0){
	    getData('class/' + this.props.class + '/album').then(json => {
		    let data = json.val();
		    let { photos } = this.state;
		    for(let i=0 ; i<data.length ; i++){
		    	photos.push({photosName: data[i].name, photosImage: this.state.server + data[i].photos[0], data: i});
		    }
		    photos.push({data: 'add'});

	      this.setState({
					photos: photos,
					permissions: this.props.permissions
	      });
	    });
    }else if(this.props.permissions == 1){
	    getData('users/' + this.props.user).then((json) => {
		    let data = json.val();
		    let { photos } = this.state;
		    photos.push({photosName: '有' + data.name + '在內的相片', photosImage: this.state.server + data.album[0], data: 'album'});

	      this.setState({
					photos: photos
	      });
	    });

	    getData('class/' + this.props.class + '/album').then(json => {
		    let data = json.val();
		    let { photos } = this.state;
		    for(let i=0 ; i<data.length ; i++){
		    	photos.push({photosName: data[i].name, photosImage: this.state.server + data[i].photos[0], data: i});
		    }

	      this.setState({
					photos: photos,
					permissions: this.props.permissions
	      });
	    });
    }
	}

	photos(item, index) {
		if(item.data == 'add') {
	    this.props.navigator.push({
	      screen: 'Photos',
	      passProps: {
	        user: this.props.user,
	        permissions: this.props.permissions,
	        data: item.data,
	        class: this.props.class,
	        index: index
	      }
	    });
		}else if(item.data == 'album') {
	    this.props.navigator.push({
	      screen: 'Balbum',
	      passProps: {
	        user: this.props.user,
	        permissions: this.props.permissions
	      }
	    });
		}else{
	    this.props.navigator.push({
	      screen: 'Photos',
	      passProps: {
	        user: this.props.user,
	        permissions: this.props.permissions,
	        data: item.data,
	        class: this.props.class
	      }
	    });
		}
	}

	updatePhoto() {
		fetch(this.state.webAPI + '/updatePhoto', {
		  method: 'POST',
		  headers: {
				'Accept': 'application/json',
		    'Content-Type': 'application/json',
		  },
		  body: JSON.stringify({
		    classname: this.props.class
		  })
		}).then(response => {
			console.log(response);
			if(response.status == 200){
        this.props.navigator.showSnackbar({
          text: 'Album update !!!!',
          textColor: '#AAA'
        });
			}
		})
    .catch(error => {
      console.error(error);
    });
	}

	render() {
		return (
			<View style={{flex: 1,backgroundColor:'#DB9D9D'}}>
				<View style={{top:10,left:148}}>
					<Text style={{fontSize:30, color: 'black'}}>
					相簿
					</Text>
				</View>
				<TouchableOpacity onPress={() => home(this.props.navigator, this.props.user, this.props.permissions)} style={{position: 'absolute',left:10,top:0}}>
					<Image style={{width:92*0.7, height: 69*0.7}} source={require('../../img/house.png')} />
				</TouchableOpacity>
				<TouchableOpacity onPress={() => menu(this.props.navigator, this.props.user, this.props.permissions)} style={{position: 'absolute',right:3,top:7}}>
					<Image style={{width:138*0.7, height: 56*0.7}} source={require('../../img/menu3.png')} />
				</TouchableOpacity>
				<TouchableOpacity onPress={() => back(this.props.navigator)} style={{position: 'absolute',left:10,top:50}}>
					<Image style={{width:45, height:35}} source={require('../../img/gobback.png')} />
				</TouchableOpacity>
				{this.props.permissions == 0 ?
				<TouchableOpacity onPress={() => this.updatePhoto()} style={{position: 'absolute',right: 20,top:50}}>
					<Ionicons name='md-sync' size={40} color='#333' />
				</TouchableOpacity>: null}

				{
					this.state.permissions == -1?
						null
					:
					<ScrollView style={{width: 360, height: 480, position: 'absolute', top: 85}}>	
						<GridList
							renderItem={({item, index}) => 
								<TouchableOpacity
									style={{width: 160, height: 160, margin: 10}}
									onPress={() => this.photos(item, index)}
									key={index}
								>
									{item.data == 'add'?
								    <View style={{width: 160, height: 160, alignItems: 'center', justifyContent: 'center', borderStyle: 'dotted', borderWidth: 1, borderColor: '#666'}}>
								    	<Ionicons name='ios-add' size={80} color='#333' />
								    </View>
										:
										<View style={{width: 160, height: 160, alignItems: 'center', justifyContent: 'center', borderStyle: 'dotted', borderWidth: 1, borderColor: '#666'}}>
								    	<Image style={{width: 140, height: 100}} source={{uri: item.photosImage}}/>
								    	<View style={{width: 140, height: 40, alignItems: 'center', justifyContent: 'center'}}>
								    	<Text style={{fontSize: 20, color: 'black'}}>{item.photosName}</Text>
								    	</View>
										</View>
									}
								</TouchableOpacity>
						  }
							data={this.state.photos}
							numColumns={2}
						/>
					</ScrollView>
				}
			</View>
		);
	}
}

const styles = StyleSheet.create({

});
import React, { Component } from 'react';
import {
	StyleSheet,
	View,
	Image,
	TouchableOpacity,
} from 'react-native';
import { Hoshi } from 'react-native-textinput-effects';

import { menu, home, back } from '../complex.js';
import { pushData, getData } from '../firebase.js';

let userJson = {
	weight: '',
	permissions: 1,
	name: '',
	height: '',
	health: '',
	family: [{
		name: '',
		phone: '',
		title: ''
	}],
	drugAllergy: '',
	birthday: '',
	album: [''],
	address: '',
	personalPhoto: ['']
};

export default class example extends Component {
	constructor(props) {
		super(props);
		this.props.navigator.toggleNavBar({
			to: 'hidden',
			animated: false,
		});
		this.sign.bind(this);
		this.state = {
			no: '',
			user: '',
			pass: ''
		};
	}

	componentDidMount() {
    getData('webAPI').then((json) => {
      this.setState({webAPI: json.val()});
    });
    getData('faceData').then((json) => {
      this.setState({faceData: json.val()});
    });
	}

	sign() {
		if(this.state.no != '' && this.state.user != '' && this.state.pass != ''){
			let temp = true;
			let {faceData} = this.state;

			faceData.map(item => {
				if(item.name == this.state.user){
					temp = false;
				}
			});

			if(temp){
				userJson['no'] = this.state.no;
				userJson['user'] = this.state.user;
				userJson['password'] = this.state.pass;
				userJson['class'] = this.props.class;

				let json = {};
				json[this.state.user] = userJson;
				pushData('users', json).then(() => {
					getData('class/' + this.props.class + '/students').then(json => {
						let student = json.val();

						student.push(this.state.user);

						pushData('class/' + this.props.class + '/students', student).then(() => {
			        fetch(this.state.webAPI + '/addPerson', {
			          method: 'POST',
			          headers: {
			            'Accept': 'application/json',
			            'Content-Type': 'application/json',
			          },
			          body: JSON.stringify({
			            user: this.state.user
			          })
			        });

					    this.props.navigator.resetTo({
					      screen: 'Home',
					      passProps: {
				          user: this.props.user,
				          permissions: this.props.permissions,
				          student: this.state.user
					      }
					    });
						});
					});
				});
			}
		}
	}

	render() {
		return (
			<View style={{alignItems: 'center'}}>
				<Image style={{width:360, height: 592}} source={require('../../img/back7.png')} />
				<TouchableOpacity onPress={() => back(this.props.navigator)} style={{position: 'absolute',left:10,top:10}}>
					<Image style={{width:45*1.2, height:35*1.2}} source={require('../../img/gobback.png')} />
				</TouchableOpacity>
				<TouchableOpacity onPress={() => this.sign()} style={{position: 'absolute',right:10,top:10}}>
					<Image style={{width:45*1.2, height:35*1.2}} source={require('../../img/gobback2.png')} />
				</TouchableOpacity>
				<View style={{top: 200, width: 280, height: 240, position: 'absolute', backgroundColor: 'white', borderWidth: 1, borderRadius: 20, borderColor: '#999'}}>
					<View style={{flex: 1, paddingLeft: 10, paddingRight: 10}}>
						<Hoshi label='學號：' borderColor='#AAA' value={this.state.no} onChangeText={(no) => this.setState({no})} />
					</View>
					<View style={{flex: 1, borderTopWidth: 0.5, paddingLeft: 10, paddingRight: 10}}>
						<Hoshi label='帳號：' borderColor='#AAA' value={this.state.user} onChangeText={(user) => this.setState({user})} />
					</View>
					<View style={{flex: 1, borderTopWidth: 0.5, paddingLeft: 10, paddingRight: 10}}>
						<Hoshi label='密碼：' borderColor='#AAA' value={this.state.pass} onChangeText={(pass) => this.setState({pass})} />
					</View>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({

});
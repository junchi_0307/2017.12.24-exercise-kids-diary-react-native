import React, { Component } from 'react';
import {
	StyleSheet,
	View,
	TouchableOpacity,
	Image,
	Text,
	ScrollView,
	TextInput
} from 'react-native';
import GridList from 'react-native-grid-list';
import Ionicons from 'react-native-vector-icons/Ionicons';

import { getData, pushData } from '../firebase.js';
import { uploadFacePhotoList } from '../uploadPhoto.js';
import { menu, home, back } from '../complex.js';

export default class example extends Component {
	constructor(props) {
		super(props);
		this.props.navigator.toggleNavBar({
			to: 'hidden',
			animated: false,
		});
		this.Bphotos.bind(this);
		this.photosName.bind(this);
		this.state = {
			permissions: -1
		};
	}

	componentDidMount() {
    getData('ip').then((json) => {
      this.setState({server: json.val()});
    });
    getData('webAPI').then((json) => {
      this.setState({webAPI: json.val()});
    });

    if(this.props.data == 'add'){
			getData('class/' + this.props.class + '/album').then(json => {
				let data = json.val();
				data.push({name: '相簿' + (this.props.index+1), photos: ['']});
				pushData('class/' + this.props.class + '/album', data);

				this.setState({
					photosName: '相簿' + (this.props.index+1),
					photos: ['add'],
					permissions: this.props.permissions
				});
			});
    }else{
			getData('class/' + this.props.class + '/album/' + this.props.data).then(json => {
				let data = json.val();
				let photos = [];

				for(let i=0 ; i<data.photos.length ; i++){
					if(data.photos[i] != ''){
						photos.push(this.state.server + data.photos[i]);
					}
				}
		    if(this.props.permissions == 0){
		    	photos.push('add');
		    }

				this.setState({
					photosName: data.name,
					photos: photos,
					permissions: this.props.permissions
				});
			});
    }
	}

  Bphotos(uri, index) {
  	if(uri == 'add') {
			uploadFacePhotoList(
				this.state.server,
				'/Server/classroom/' + this.props.class + '/album/' + this.props.data,
				'class/' + this.props.class + '/album/' + this.props.data + '/photos',
				index,
				(uri) => {
					getData('class/' + this.props.class + '/album/' + this.props.data).then(json => {
						let data = json.val();
						let photos = [];

						for(let i=0 ; i<data.photos.length ; i++){
							if(data.photos[i] != ''){
								photos.push(this.state.server + data.photos[i]);
							}
						}
				    if(this.props.permissions == 0){
				    	photos.push('add');
				    }

						this.setState({
							photosName: data.name,
							photos: photos
						});

						console.log(this.state.webAPI + '/updateFace');
						console.log(this.state.server + uri);
						fetch(this.state.webAPI + '/updateFace', {
						  method: 'POST',
						  headers: {
  							'Accept': 'application/json',
						    'Content-Type': 'application/json',
						  },
						  body: JSON.stringify({
						    uri: this.state.server + uri
						  })
						});
					});
				}
			);
  	} else {
	    this.props.navigator.push({
	      screen: 'Bphotos',
	      passProps: {
	        user: this.props.user,
	        permissions: this.props.permissions,
	        uri: uri,
	        imageIndex: index
	      }
	    });
  	}
  }

  photosName(photosName) {
  	pushData('class/' + this.props.class + '/album/' + this.props.data, {name: photosName});
  	this.setState({photosName});
  }

	render() {
		let { permissions } = this.state;
		console.log(this.state);
		return (
			<View style={{flex: 1,backgroundColor:'#DB9D9D'}}>
				<View style={{top:10, left:70, width: 190}}>
					{
						permissions == -1?
							null
						:
						permissions == 0?
							<TextInput style={{fontSize:30, color: 'black'}} value={this.state.photosName} onChangeText={(photosName) => this.photosName(photosName)} />
						:
							<Text style={{fontSize:30, color: 'black'}}>
								{this.state.photosName}
							</Text>
					}
				</View>
				<TouchableOpacity onPress={() => home(this.props.navigator, this.props.user, this.props.permissions)} style={{position: 'absolute',left:10,top:0}}>
					<Image style={{width:92*0.7, height: 69*0.7}} source={require('../../img/house.png')} />
				</TouchableOpacity>
				<TouchableOpacity onPress={() => menu(this.props.navigator, this.props.user, this.props.permissions)} style={{position: 'absolute',right:3,top:7}}>
					<Image style={{width:138*0.7, height: 56*0.7}} source={require('../../img/menu3.png')} />
				</TouchableOpacity>
				<TouchableOpacity onPress={() => back(this.props.navigator)} style={{position: 'absolute',left:10,top:50}}>
					<Image style={{width:45, height:35}} source={require('../../img/gobback.png')} />
				</TouchableOpacity>
				{
					permissions == -1?
						null
					:
					<ScrollView style={{width: 360, height: 480, position: 'absolute', top: 85}}>	
						<GridList
							renderItem={({item, index}) => 
								<TouchableOpacity
									style={{width: 160, height: 160, margin: 10}}
									onPress={() => this.Bphotos(item, index)}
									key={index}
								>
									{item == 'add'?
								    <View style={{width: 160, height: 160, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center', opacity: 0.7}}>
								    	<Ionicons name='ios-add' size={80} color='#999' />
								    </View>
										:
								    <Image style={{width: 160, height: 160}} source={{uri: item}}/>	
									}
								</TouchableOpacity>
						  }
							data={this.state.photos}
							numColumns={2}
						/>
					</ScrollView>
				}
			</View>
		);
	}
}

const styles = StyleSheet.create({

});
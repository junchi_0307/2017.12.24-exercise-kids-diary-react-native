import React, { Component } from 'react';
import {
	StyleSheet,
	View,
	TouchableOpacity,
	Image,
	Text,
	ScrollView
} from 'react-native';
import GridList from 'react-native-grid-list';

import { getData } from '../firebase.js';
import { menu, home, back } from '../complex.js';

export default class example extends Component {
	constructor(props) {
		super(props);
		this.props.navigator.toggleNavBar({
			to: 'hidden',
			animated: false,
		});
		this.Bphotos.bind(this);
		this.state = {
		};
	}

	componentDidMount() {
    getData('ip').then((json) => {
      this.setState({server: json.val()});
    });
    getData('users/' + this.props.user + '/album').then((json) => {
	    let data = json.val();
	    console.log(data);

	    for(let i=0 ; i<data.length ; i++){
	    	if(data[i] != ''){
	    		data[i] = this.state.server + data[i];
	    	}
	    }

      this.setState({
        permissions: this.props.permissions,
        album: data,
      });
    });
	}

  Bphotos(uri, index) {
    this.props.navigator.push({
      screen: 'Bphotos',
      passProps: {
        user: this.props.user,
        permissions: this.props.permissions,
        uri: uri,
        imageIndex: index
      }
    });
  }

	render() {
		return (
			<View style={{flex: 1,backgroundColor:'#DB9D9D'}}>
				<View style={{top:10,left:148}}>
					<Text style={{fontSize:30, color: 'black'}}>
					相簿
					</Text>
				</View>
				<TouchableOpacity onPress={() => home(this.props.navigator, this.props.user, this.props.permissions)} style={{position: 'absolute',left:10,top:0}}>
					<Image style={{width:92*0.7, height: 69*0.7}} source={require('../../img/house.png')} />
				</TouchableOpacity>
				<TouchableOpacity onPress={() => menu(this.props.navigator, this.props.user, this.props.permissions)} style={{position: 'absolute',right:3,top:7}}>
					<Image style={{width:138*0.7, height: 56*0.7}} source={require('../../img/menu3.png')} />
				</TouchableOpacity>
				<TouchableOpacity onPress={() => back(this.props.navigator)} style={{position: 'absolute',left:10,top:50}}>
					<Image style={{width:45, height:35}} source={require('../../img/gobback.png')} />
				</TouchableOpacity>
				{
					this.state.permissions == -1?
						null
					:
					<ScrollView style={{width: 360, height: 480, position: 'absolute', top: 85}}>	
						<GridList
							renderItem={({item, index}) => 
								<TouchableOpacity
									style={{width: 160, height: 160, margin: 10}}
									onPress={() => this.Bphotos(item, index)}
									key={index}
								>
									{item == 'add'?
								    <View style={{width: 160, height: 160, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center', opacity: 0.7}}>
								    	<Ionicons name='ios-add' size={80} color='#999' />
								    </View>
										:
								    <Image style={{width: 160, height: 160}} source={{uri: item}}/>	
									}
								</TouchableOpacity>
						  }
							data={this.state.album}
							numColumns={2}
						/>
					</ScrollView>
				}
			</View>
		);
	}
}

const styles = StyleSheet.create({

});
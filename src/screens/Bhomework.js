import React, { Component } from 'react';
import {
	StyleSheet,
	View,
	TouchableOpacity,
	Image,
	Text,
} from 'react-native';
import PhotoView from 'react-native-photo-view';
import { menu, home, back } from '../complex.js';
import { downloadFile } from '../downloadFile.js';

export default class example extends Component {
	constructor(props) {
		super(props);
		this.props.navigator.toggleNavBar({
			to: 'hidden',
			animated: false,
		});
		this.state = {

		};
	}

	render() {
		return (
			<View style={{flex: 1,backgroundColor:'#F1CD94'}}>
				<View style={{top:6,left:105}}>
					<Text style={{fontSize:30, color: 'black'}}>
						學習作業單
					</Text>
				</View>
				<TouchableOpacity onPress={() => home(this.props.navigator, this.props.user, this.props.permissions)} style={{position: 'absolute',left:10,top:0}}>
					<Image style={{width:92*0.7, height: 69*0.7}} source={require('../../img/house.png')} />
				</TouchableOpacity>
				<TouchableOpacity onPress={() => menu(this.props.navigator, this.props.user, this.props.permissions)} style={{position: 'absolute',right:3,top:7}}>
					<Image style={{width:138*0.7, height: 56*0.7}} source={require('../../img/menu3.png')} />
				</TouchableOpacity>
				<TouchableOpacity onPress={() => back(this.props.navigator)} style={{position: 'absolute',left:10,top:50}}>
					<Image style={{width:45, height:35}} source={require('../../img/gobback.png')} />
				</TouchableOpacity>
				<TouchableOpacity onPress={() => downloadFile(this.props.uri, this.props.navigator)} style={{position: 'absolute',right:10,top:510}}>
					<Image style={{width:195*0.2, height:221*0.2}} source={require('../../img/download.png')} />
				</TouchableOpacity>
				<View style={{top: 85, width: 360, height: 507, position: 'absolute'}}>
					<PhotoView
					  source={{uri: this.props.uri}}
					  minimumZoomScale={1}
					  maximumZoomScale={3}
					  androidScaleType="fitCenter"
					  onLoad={() => console.log("Image loaded!")}
					  style={{width: 360, height: 420}}
					/>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({

});
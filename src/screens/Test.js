import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  AlertIOS
} from 'react-native';
 
//图片选择器
import * as ImagePicker from 'react-native-image-picker';

import { FileUpload } from 'NativeModules';
 
//图片选择器参数设置
let options = {
  title: '请选择图片来源',
  cancelButtonTitle:'取消',
  takePhotoButtonTitle:'拍照',
  chooseFromLibraryButtonTitle:'相册图片',
  customButtons: [
    {name: 'hangge', title: 'hangge.com图片'},
  ],
  storageOptions: {
    skipBackup: true,
    path: 'images'
  }
};

export default class Test extends Component {
  constructor(props) {
    super(props);
    this.testUploadPhoto.bind(this);
    this.fetchUploadPhoto.bind(this);
    this.state = {
      avatarSource: null
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.item} onPress={this.choosePic.bind(this)}>选择照片</Text>
        <Image source={this.state.avatarSource} style={styles.image} />
      </View>
    );
  }

  componentDidMount(){
  }

  testUploadPhoto(fileName, filePath, fileType){
    let obj = {
          uploadUrl: 'http://61.224.102.140:8080/Server/face/',
          method: 'POST', // default 'POST',support 'POST' and 'PUT' 
          headers: {
            'Accept': 'application/json',
            'Authorization': 'Basic YXBwOmtpZHNfZGlhcnk='
          },
          fields: {
              'hello': 'world',
          },
          files: [
            {
              //name: 'one', // optional, if none then `filename` is used instead 
              filename: fileName, // require, file name 
              filepath: filePath, // require, file absoluete path 
              filetype: fileType, // options, if none, will get mimetype from `filepath` extension 
            },
          ]
    };
    console.log(JSON.stringify(obj));
    FileUpload.upload(obj, (err, result) => {
      console.log('upload:', err, result);
    });
  }

  fetchUploadPhoto(){
    const data = new FormData();
    data.append('name', 'kids'); // you can append anyone.
    data.append('photo', {
      uri: photo.uri,
      type: 'image/jpeg', // or photo.type
      name: 'testPhotoName'
    });
    fetch(url, {
      method: 'post',
      body: data
    }).then(res => {
      console.log(res)
    });
  }

   //选择照片按钮点击
  choosePic() {
    //http://www.hangge.com/blog/cache/detail_1617.html
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('用户取消了选择！');
      }
      else if (response.error) {
        alert("ImagePicker发生错误：" + response.error);
      }
      else if (response.customButton) {
        alert("自定义按钮点击：" + response.customButton);
      }
      else {
        let source = { uri: response.uri };
        this.testUploadPhoto(response.fileName, response.path, response.type);
        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };
        this.setState({
          avatarSource: source
        });
      }
    });
  }
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
    marginTop:25
  },
  item:{
    margin:15,
    height:30,
    borderWidth:1,
    padding:6,
    borderColor:'#ddd',
    textAlign:'center'
  },
  image:{
   height:198,
   width:300,
   alignSelf:'center',
 },
});
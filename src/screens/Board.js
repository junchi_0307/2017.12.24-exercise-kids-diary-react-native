import React, { Component } from 'react';
import {
	StyleSheet,
	View,
	Image,
	Text,
	TouchableOpacity,
	ScrollView,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

import { getData, pushData } from '../firebase.js';
import { menu, back, home } from '../complex.js';

export default class example extends Component {
	constructor(props) {
		super(props);
		this.props.navigator.toggleNavBar({
			to: 'hidden',
			animated: false,
		});
    this.board2.bind(this);
    this.addBoard.bind(this);
		this.state = {
			news: []
		};
	}

  board2(index) {
    this.props.navigator.push({
      screen: 'Board2',
      passProps: {
        user: this.props.user,
        permissions: this.props.permissions,
        index: index,
        class: this.props.class
      }
    });
  }

  addBoard() {
    getData('class/' + this.props.class + '/news').then((json) => {
    	let news = json.val();
    	let index = json.val().length;
    	news.push({article: '', date: '', title: ''});
    	pushData('class/' + this.props.class + '/news', news).then(() => {
	    this.props.navigator.push({
	      screen: 'Board2',
	      passProps: {
	        user: this.props.user,
	        permissions: this.props.permissions,
	        index: index,
	        class: this.props.class
	      }
	    });
    	});
    });
  }

	componentDidMount() {
    getData('ip').then((json) => {
      this.setState({server: json.val()});
    });
    getData('class/' + this.props.class).then((json) => {
	    let data = json.val();
	    let news = [];
	    for(let i=data.news.length-1 ; i>=0 ; i--){
				news.push(data.news[i]);
				news[data.news.length-1-i]['no'] = i;
	    	console.log(news[i]);
	    }

	    for(let i=news.length ; i<6 ; i++){
	    	news.push({ date: 'null', title: 'null', article:'null' });
	    }

      this.setState({
        permissions: this.props.permissions,
        news: news
      });
    });
	}

	render() {
    console.log(this.state);
		return (
			<View style={{alignItems: 'center'}}>
				<Image style={{width:360, height: 592}} source={require('../../img/Back3.gif')} />
				<ScrollView style={{width: 280, height: 320, top: 120,position: 'absolute',backgroundColor: 'white', borderWidth: 0.5}}>
					<View style={{width: 280, height: this.state.news.lenght * 50 + 20, padding: 10}}>
						{this.state.news.map((data, index) => {
							return(
								<View key={index} style={[{width: 260, height: 50}, index==0?styles.newsOne:styles.news]}>
									{data.title != 'null' && data.date != 'null' && data.article != 'null'?
										<TouchableOpacity onPress={() => {this.board2(data.no)}} style={{flex: 1, flexDirection: 'row'}}>
											<View style={{flex: 8, alignItems: 'center', justifyContent: 'center'}}>
												<Text style={{fontSize: 18}}>{data.title.length<=8?data.title:data.title.substring(0, 8) + '...'}</Text>
											</View>
											<View style={{flex: 3, borderLeftWidth: 0.25, alignItems: 'center', justifyContent: 'center'}}>
												<Text style={{fontSize: 18}}>{data.date.substring(5)}</Text>
											</View>
										</TouchableOpacity>
										:
											<View style={{flex: 1, flexDirection: 'row'}}>
												<View style={{flex: 8, alignItems: 'center', justifyContent: 'center'}}>
													<Text style={{fontSize: 18}}></Text>
												</View>
												<View style={{flex: 3, borderLeftWidth: 0.25, alignItems: 'center', justifyContent: 'center'}}>
													<Text style={{fontSize: 18}}></Text>
												</View>
											</View>
									}
								</View>
							)
						})}
					</View>
				</ScrollView>
				<TouchableOpacity onPress={() => home(this.props.navigator, this.props.user, this.props.permissions)} style={{position: 'absolute',left:15,top:0}}>
					<Image style={{width:92*0.8, height: 69*0.8}} source={require('../../img/house.png')} />
				</TouchableOpacity>
				<TouchableOpacity onPress={() => back(this.props.navigator)} style={{position: 'absolute',left:90,top:10}}>
					<Image style={{width:45*1.2, height:35*1.2}} source={require('../../img/gobback.png')} />
				</TouchableOpacity>
				<TouchableOpacity onPress={() => menu(this.props.navigator, this.props.user, this.props.permissions)} style={{position: 'absolute',right:25,top:7}}>
					<Image style={{width:138*0.8, height: 56*0.8}} source={require('../../img/menu3.png')} />
				</TouchableOpacity>
				<View style={{top:70,left:120,position: 'absolute',borderBottomColor: 'black',borderBottomWidth: 1 }}>
    			<Text style={{fontSize:30, color: 'black'}}>
      		最新消息
    			</Text>
      	</View>
      	{this.props.permissions == 0?
      		<TouchableOpacity onPress={() => this.addBoard()} style={{top:70, right:40, width: 40, height: 40, position: 'absolute' }}>
					<Ionicons name='md-add-circle' size={40} color='#999' />
      		</TouchableOpacity>: null
      	}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	newsOne: {

	},
	news: {
		borderTopWidth: 0.25
	}
});
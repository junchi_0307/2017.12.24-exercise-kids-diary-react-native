import React, { Component } from 'react';
import {
	StyleSheet,
	View,
	Image,
	Text,
	TouchableOpacity,
} from 'react-native';

import { getData } from '../firebase.js';
import { menu, home, back } from '../complex.js';

export default class example extends Component {
	constructor(props) {
		super(props);
		this.props.navigator.toggleNavBar({
			to: 'hidden',
			animated: false,
		});
		this.check.bind(this);
		this.homework.bind(this);
		this.coner.bind(this);
		this.works.bind(this);
		this.album.bind(this);
		this.state = {
			permissions: -1
		};
	}

	componentDidMount() {
    getData('ip').then((json) => {
      this.setState({server: json.val()});
    });
    getData('users/' + this.props.user).then((json) => {
	    let data = json.val();

      this.setState({
        permissions: this.props.permissions,
        class: data.class
      });
    });
	}

  check(){
    this.props.navigator.push({
      screen: 'Check',
      passProps: {
        user: this.props.user,
        permissions: this.props.permissions,
        class: this.state.class
      }
    });
  }

  homework(){
    this.props.navigator.push({
      screen: 'Homework',
      passProps: {
        user: this.props.user,
        permissions: this.props.permissions,
        class: this.state.class
      }
    });
  }

  coner(){
    this.props.navigator.push({
      screen: 'Coner',
      passProps: {
        user: this.props.user,
        permissions: this.props.permissions,
        class: this.state.class
      }
    });
  }

  works(){
    this.props.navigator.push({
      screen: 'Works',
      passProps: {
        user: this.props.user,
        permissions: this.props.permissions,
        class: this.state.class
      }
    });
  }

  album(){
    this.props.navigator.push({
      screen: 'Album',
      passProps: {
        user: this.props.user,
        permissions: this.props.permissions,
        class: this.state.class
      }
    });
  }

	render() {
		return (
			<View style={{alignItems: 'center'}}>
				{this.state.permissions == -1?
					null
				:
					<View style={{alignItems: 'center'}}>
						<Image style={{width:360, height: 592}} source={require('../../img/Back3.gif')} />
						<TouchableOpacity onPress={() => home(this.props.navigator, this.props.user, this.props.permissions)} style={{position: 'absolute',left:15,top:0}}>
							<Image style={{width:92*0.8, height: 69*0.8}} source={require('../../img/house.png')} />
						</TouchableOpacity>
						<TouchableOpacity onPress={() => menu(this.props.navigator, this.props.user, this.props.permissions)} style={{position: 'absolute',right:25,top:7}}>
							<Image style={{width:138*0.8, height: 56*0.8}} source={require('../../img/menu3.png')} />
						</TouchableOpacity>
						<TouchableOpacity onPress={() => this.check()} style={{position: 'absolute',right:40,top:60}}>
							<Image style={{width:973*0.15, height: 1034*0.15}} source={require('../../img/check2.png')} />
						</TouchableOpacity>
						<TouchableOpacity onPress={() => this.homework()} style={{position: 'absolute',left:25,top:120}}>
							<Image style={{width:1201*0.155, height: 1189*0.155}} source={require('../../img/homework.png')} />
						</TouchableOpacity>
						<TouchableOpacity onPress={() => this.coner()} style={{position: 'absolute',right:20,top:215}}>
							<Image style={{width:1092*0.155, height: 1076*0.155}} source={require('../../img/coner.png')} />
						</TouchableOpacity>
						<TouchableOpacity onPress={() => this.works()} style={{position: 'absolute',left:20,top:300}}>
							<Image style={{width:1155*0.155, height: 1071*0.155}} source={require('../../img/works.png')} />
						</TouchableOpacity>
						<TouchableOpacity onPress={() => this.album()} style={{position: 'absolute',right:20,top:380}}>
							<Image style={{width:1205*0.155, height: 1080*0.155}} source={require('../../img/album.png')} />
						</TouchableOpacity>
					</View>
				}
			</View>

		);
	}
}
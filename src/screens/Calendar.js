import React, { Component } from 'react';
import {Agenda} from 'react-native-calendars';
import {
	StyleSheet,
	View,
	Image,
	Text,
	TouchableOpacity,
} from 'react-native';

import { getData } from '../firebase.js';
import { menu, home, back } from '../complex.js';

export default class example extends Component {
	constructor(props) {
		super(props);
		this.props.navigator.toggleNavBar({
			to: 'hidden',
			animated: false,
		});
    this.addCalendar.bind(this);
		this.state = {
			items:{}
		};
	}

  componentDidMount() {
    //{'2017-11-08': [{text: ''}]}
    //{'2017-11-08': [{textColor: 'red'}]}
    getData('class/' + this.props.class + '/calendar').then(json => {
      let data = json.val();
      let items = {};
      let marked = {};
      data.map(item => {
        items[item.date] = [{text: item.text + '\n地點：' + item.address}];
        marked[item.date] = [{textColor: 'red'}];
      });
      this.setState({items, marked});
    });
  }

  addCalendar() {
    this.props.navigator.push({
      screen: 'Addcalendar',
      passProps: {
        user: this.props.user,
        permissions: this.props.permissions,
        class: this.props.class
      }
    });
  }

	loadItems(day) {
    setTimeout(() => {
      for (let i = -1; i < 3; i++) {
        const time = day.timestamp + i * 24 * 60 * 60 * 1000;
        const strTime = this.timeToString(time);
        if (!this.state.items[strTime]) {
          this.state.items[strTime] = [];
        }
      }
      console.log(this.state.items);
      const newItems = {};
      Object.keys(this.state.items).forEach(key => {newItems[key] = this.state.items[key];});
      this.setState({
        items: newItems
      });
    }, 1000);
    // console.log(`Load Items for ${day.year}-${day.month}`);
  }

  renderItem(item) {
    return (
      <View style={[styles.item, {height: item.height}]}><Text>{item.text}</Text></View>
    );
  }

  renderEmptyDate() {
    return (
      <View style={styles.emptyDate}><Text>This is empty date!</Text></View>
    );
  }

  rowHasChanged(r1, r2) {
    return r1.name !== r2.name;
  }


  timeToString(time) {
    const date = new Date(time);
    return date.toISOString().split('T')[0];
  }

	render() {
		return (
			<View style={{alignItems: 'center'}}>
				<Image style={{width:360, height: 592}} source={require('../../img/back6.png')} />
				<View style={{top:10,left:135,position: 'absolute'}}>
					<Text style={{fontSize:30, color: 'black'}}>
						行事曆
					</Text>
				</View>
				<TouchableOpacity onPress={() => home(this.props.navigator, this.props.user, this.props.permissions)} style={{position: 'absolute',left:10,top:0}}>
					<Image style={{width:92*0.7, height: 69*0.7}} source={require('../../img/house.png')} />
				</TouchableOpacity>
				<TouchableOpacity onPress={() => menu(this.props.navigator, this.props.user, this.props.permissions)} style={{position: 'absolute',right:3,top:7}}>
					<Image style={{width:138*0.7, height: 56*0.7}} source={require('../../img/menu3.png')} />
				</TouchableOpacity>
				<TouchableOpacity onPress={() => back(this.props.navigator)} style={{position: 'absolute',left:10,top:50}}>
					<Image style={{width:45, height:35}} source={require('../../img/gobback.png')} />
				</TouchableOpacity>
        {this.props.permissions == 0?
				<TouchableOpacity onPress={() => this.addCalendar()} style={{position: 'absolute',right:10,top:80}}>
					<Image style={{width:189*0.2, height:189*0.2}} source={require('../../img/add.png')} />
				</TouchableOpacity>
        :null}
				<View style={{width:360, height:500,position: 'absolute',top:130,left:0}}>

      <Agenda
        items={this.state.items}
        loadItemsForMonth={this.loadItems.bind(this)}
        selected={'2017-12-28'}
        renderItem={this.renderItem.bind(this)}
        renderEmptyDate={this.renderEmptyDate.bind(this)}
        rowHasChanged={this.rowHasChanged.bind(this)}
        //markingType={'interactive'}
        markedDates={
          this.state.marked
        }
        //  '2017-05-09': [{textColor: '#666'}],
        //  '2017-05-14': [{startingDay: true, color: 'blue'}, {endingDay: true, color: 'blue'}],
        //  '2017-05-21': [{startingDay: true, color: 'blue'}],
        //  '2017-05-22': [{endingDay: true, color: 'gray'}],
        //  '2017-05-24': [{startingDay: true, color: 'gray'}],
        //  '2017-05-25': [{color: 'gray'}],
        //  '2017-05-26': [{endingDay: true, color: 'gray'}]}}
        // monthFormat={'yyyy'}
        // theme={{calendarBackground: 'red', agendaKnobColor: 'green'}}
        //renderDay={(day, item) => (<Text>{day ? day.day: 'item'}</Text>)}
  		pastScrollRange = { 15 }
  		futureScrollRange = { 15 }
      />

				</View>


			</View>

		);
	}
}


const styles = StyleSheet.create({
  item: {
    backgroundColor: 'white',
    flex: 1,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 17
  },
  emptyDate: {
    height: 15,
    flex:1,
    paddingTop: 30
  }
});
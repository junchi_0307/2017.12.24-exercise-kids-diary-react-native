import {Navigation} from 'react-native-navigation';

// screen related book keeping
import {registerScreens} from './screens';
registerScreens();

Navigation.startSingleScreenApp({
	screen: {
		screen: 'Mean',//Mean
		navigatorStyle: {
			orientation: 'portrait'
		}
	},
	animationType: 'fade'
});